from ghg_emissions import db
import csv

class Countries(db.Model):
	__tablename__ = 'countries'
	id            = db.Column(db.Integer, primary_key=True)
	country_name  = db.Column(db.String(30),nullable=False)

class Categories(db.Model):
	__tablename__ = 'categories'
	id            = db.Column(db.Integer, primary_key=True)
	category_name = db.Column(db.String(60),nullable=False)

class GhgData(db.Model):
	__tablename__ = 'ghg_data'
	id            = db.Column(db.Integer,primary_key=True)
	country_id    = db.Column(db.Integer,db.ForeignKey('countries.id'))
	year          = db.Column(db.Integer,nullable=False)
	value         = db.Column(db.Float,nullable=False)
	category_id   = db.Column(db.Integer,db.ForeignKey('categories.id'))

class GhgInventoryData(db.Model):
	__tablename__   = 'ghg_inventory_data'
	id              = db.Column(db.Integer,primary_key=True)
	country_or_area = db.Column(db.String(60))
	year            = db.Column(db.Integer)
	value           = db.Column(db.Float)
	category        = db.Column(db.String(200))

db.create_all()

