from .models import *
import csv

results = []
country_set  = set()
category_set = set()
list_of_area = ['European Union','Russian Federation']

ghg_category_map = {
	"carbon_dioxide_co2_emissions_without_land_use_land_use_change_and_forestry_lulucf_in_kilotonne_co2_equivalent":"CO2",
	"greenhouse_gas_ghgs_emissions_including_indirect_co2_without_lulucf_in_kilotonne_co2_equivalent":"GHG-indirect-CO2",
	"greenhouse_gas_ghgs_emissions_without_land_use_land_use_change_and_forestry_lulucf_in_kilotonne_co2_equivalent":"GHG",
	"hydrofluorocarbons_hfcs_emissions_in_kilotonne_co2_equivalent":"HFC",
	"methane_ch4_emissions_without_land_use_land_use_change_and_forestry_lulucf_in_kilotonne_co2_equivalent":"CH4",
	"nitrogen_trifluoride_nf3_emissions_in_kilotonne_co2_equivalent":"HF3",
	"nitrous_oxide_n2o_emissions_without_land_use_land_use_change_and_forestry_lulucf_in_kilotonne_co2_equivalent":"N2Os",
	"perfluorocarbons_pfcs_emissions_in_kilotonne_co2_equivalent":"PFCs",
	"sulphur_hexafluoride_sf6_emissions_in_kilotonne_co2_equivalent":"F6",
	"unspecified_mix_of_hydrofluorocarbons_hfcs_and_perfluorocarbons_pfcs_emissions_in_kilotonne_co2_equivalent":"HFC-PFC-mix"
}

with open('greenhouse_gas_inventory_data_data.csv') as file:
	reader = csv.reader(file)
	for row in reader:
		results.append(row)

for data_row in results[1:]:
  country_name  = data_row[0]
  year          = int(data_row[1])
  value         = float(data_row[2])
  category      = data_row[3]

  if country_name not in country_set and country_name not in list_of_area:
    country_set.add(country_name)

  ghg_category = ghg_category_map[category]

  if ghg_category not in category_set:
    category_set.add(ghg_category)

  add_record = GhgInventoryData(
																country_or_area=country_name,
                                year=year,
                                value=value,
                                category=category
                                )
                                
  db.session.add(add_record)

for category in category_set:
  add_record = Categories(category_name=category)
  db.session.add(add_record)

for country_name in country_set:
  add_record = Countries(country_name=country_name)
  db.session.add(add_record)

for data_row in results[1:]:
  get_country_id  = Countries.query.filter_by(country_name=data_row[0]).first()
  category_name   = ghg_category_map[data_row[3]]
  get_category_id = Categories.query.filter_by(category_name=category_name).first()

  if get_country_id is None :
    continue

  country_id   = get_country_id.id
  category_id  = get_category_id.id
  
  add_record = GhgData( country_id  = country_id,  
                        year        = int(data_row[1]),
                        value       = float(data_row[2]),
                        category_id = category_id
  )
  db.session.add(add_record)
  

try:
  db.session.commit()
except Exception as e:
  db.session.rollback()
finally:
  db.session.close()





