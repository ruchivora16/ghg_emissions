from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import yaml 

with open("config.yaml","r") as s :
	config = yaml.load(s,yaml.SafeLoader)

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']        = config['development']['DATABASE_URI']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = config['development']['SQLALCHEMY_TRACK_MODIFICATIONS']

db = SQLAlchemy(app)

from ghg_emissions import models
from ghg_emissions import routes
from ghg_emissions import load_data